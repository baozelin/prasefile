import re

'''
    function has three inputs
    read file first and transfor data into a list, then traversal this list.
'''


def parseFile(file_name, mid, bar):  # for example, mid = 2, bar = 0.5
    with open(file_name, 'r') as file:
        content = file.read()
        print(type(content))
    predict_list = re.split(" ;|;|\s+", content)  # split content by space and ;

    # declare some variables
    correct_num = 0
    total_num = 0
    index = 0

    # handle list
    while index < len(predict_list):

        if len(predict_list[index]) > 7: # hardcode for length of number
            index += 1
            continue

        mid_list = []
        for x in range(mid):
            mid_list.append(predict_list[index])
            index += 1

        while index < len(predict_list) and len(predict_list[index]) < 7:   # hardcode for length of number
            if len(predict_list[index]) < 3:    # hardcode for length of number--- 0-40
                value = predict_list[index]
                index += 1

            if index < len(predict_list) and float(predict_list[index]) >= bar:
                if value in mid_list:
                    correct_num += 1

                total_num += 1

            index += 1

    correct_perct = float(correct_num) / total_num
    print(correct_perct)  # print correct percent
    return correct_perct


if __name__ == "__main__":
    parseFile("/Users/zelinbao/Documents/test_jizhong_1st.txt", 2, 0.5)
